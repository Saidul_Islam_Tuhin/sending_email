""" 
    Name
    description: scrape Email address from google spreadshhet api and sending email
    Dependency: smtplib, pygsheets

    
"""

import config
import pygsheets
import smtplib
from email.mime.text import MIMEText

#authorization spreadsheet
try:
    gc = pygsheets.authorize(service_file='test-project.json')
    sheet=gc.open_by_url('https://docs.google.com/spreadsheets/d/1Wzs71o_9uDIXhxtn_6AFWHsIXQFwdonJz7uEAA7Wlvs/edit#gid=0')
except pygsheets.SpreadsheetNotFound:
    print("dfsdf")
    raise Exception("Trying to open non-existent or inaccessible spreadsheet.Pleasi input wirte url.")
except pygsheets.AuthenticationError:
    raise Exception("An error during authentication process.")
except FileNotFoundError:
    raise Exception("secret test file is missing")

# collect data from spreadsheet
try:
    wks= sheet.sheet1
    values = wks.get_all_values()
    print(values)
except pygsheets.AuthenticationError:
    raise Exception("An error during authentication process.")


# login by provide email account and password
try:
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    server.set_debuglevel(0)

    #it run unitill user give correct email and password
    while True:
        try:
            sender_email_id, pwd = config.email_information()
            code, message = server.login(sender_email_id, pwd) # log in to the server
            if code == 235:
                break
        except Exception as e:
            print("Insert correct Email id or User name", e)
            continue
    
    # Sending email
    for count, info in enumerate(values[1:], 1):
        first_name,to_email_id,date='','',''
        first_name= (info[1]).split(" ")[0]
        print(info[1])
        #print(first_name)
        to_email_id= info[2]
        date = info[0]
        body = "Hi {} ;\nThis e-mail is to inform you that the campaign for the following unit- {} - is  ending {} \n\nWere you able to rent the units?\n\nPlease let me know if you would like to keep the ad running or if you have any other units you’d like to publicize.\n\nThank you and we look forward to working with you.".format(first_name, to_email_id, date)
        #print(body)
        msg = MIMEText(body)
        msg['From'] = sender_email_id
        msg['To'] = to_email_id
        msg['Subject'] = "End of Campaign Notification"
        text = msg.as_string()
        #print('msg send To {} and sending {} in: {}\n'.format(first_name,count,total))
        server.sendmail(sender_email_id, to_email_id, text)

except Exception as e:
    raise Exception("Unexpected error. Plz try it again.")

server.quit()
